require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test "should send email to hendra.mulyana@kiranatama.com" do
    assert_not_nil UserMailer.registration_confirmation(User.find(users(:admin))).deliver, true
  end
end
