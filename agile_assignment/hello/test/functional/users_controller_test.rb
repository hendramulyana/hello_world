require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  
  test "should get new" do
    get :new
    assert_response :success
  end
  
  test "should get create" do
    #test create user with a not matched password and confirmation
    post :create, :user => {:username => "testusername",
                   :password => "1234",
                   :password_confirmation => "123d4",
                   :first_name => "good",
                   :last_name => "day",
                   :email => "susa@cosgag.com"}
    assert_response :success
    assert_equal flash[:error], "Password and Confirmation did not match"
    
    #test create user
    post :create, :user => {:username => "testusername",
                   :password => "1234",
                   :password_confirmation => "1234",
                   :first_name => "good",
                   :last_name => "day",
                   :email => "susa@cosgag.com"}
    assert_response :redirect
    assert_redirected_to login_path
  end
  
end
