require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  
  def test_create_product
    user = User.find(users(:admin))
    product = user.products.new(:name => "test", :price => 13423, :description => "testdesc")
    assert_equal product.save, true
  end
  
  def test_price_morethan_1000
    assert_not_nil Product.price_morethan_1000
  end 

  def test_red_product
    assert_not_nil Product.get_red_product
  end 
end
