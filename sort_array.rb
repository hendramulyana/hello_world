def sort_array(arr)
  arr.each_with_index do |value, index| 
    arr.each_with_index do |value2, index2| 
      if index2 < index then next end
      if value > value2 then
        buf = arr[index]
        arr[index] = value2
        arr[index2] = buf
      end
    end
  end
  return arr.sort
end

arr = [4,52,2,5,1,25,2,5,6,2]
arr = sort_array(arr)

arr.each_with_index{ |value, index| puts "index #{index} is #{value}"}
