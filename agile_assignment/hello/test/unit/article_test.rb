require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  def test_create_comment_by_user
    article = User.find(users(:admin)).articles.new(:rating => 1, :title => "avc", :body => "fds")
    assert_equal article.save, true
  end
  
  def select_by_creator
    assert_nil creator(:admin).articles
  end
  
  def test_create_title
    user = users(:admin)
    article = user.articles.new(:body => "body")
    
    #test create without title
    assert_equal article.save, false
    
    #test create with blank title
    article.title = ""
    assert_equal article.save, false
    
    #test create ununique article
    user.articles.create(:body => "body", :title => "title")
    article = user.articles.new(:body => "body", :title => "title")
    assert_equal article.save, false
  end
  
  def test_by_rating
    assert_not_nil Article.by_rating(1)
  end
  
  def test_longerthan_100
    article = users(:admin).articles.create(:body => "bodygadghdhdffghdah6fdkgdkahkjdhgjhjghdkajhgeiuhguehwgjhdlghjehgjegjdhjgdjghdjghuehghjghdjkghjdhgjdhgjkahgdjhgjhgjhgkahgdjkhgkjahgdjhkgjhjhgjghjkghjkahgdjkahgdjkahgjkhgjhagjhagjhgjhgjhgjsghjshgjshgjsdhgjshgjkshgjkhgjhgjhgjhsgjhsgjhsgjhgjdhgjdhagjhdsgjhsg",
                                            :title => "title6")
    assert_not_nil article.longerthan_100
  end
  
end
