class Article < ActiveRecord::Base
  attr_accessible :title, :body, :rating, :user_id
  has_many :comments, :dependent => :destroy
  belongs_to :user
  belongs_to :creator,
             :class_name => "User",
             :foreign_key => "user_id",
             :conditions => "title like '%my country%'"
  
  validates :title,   :presence => true,
                      :uniqueness => true,
                      :allow_blank => false
  
  scope :by_rating, lambda {|rate| where("rating = ?", rate)}
  
  #get article that longer than 100 characters
  def longerthan_100
    buf = Article.where("CHAR_LENGTH(body) > 100")
    buf.each do |data|
      puts "Title : #{data.title} Length : #{data.body.length}"
    end     
  end
end
