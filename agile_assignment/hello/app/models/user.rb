class User < ActiveRecord::Base
  
  before_save :encrypt_password
  
  #attribute
  attr_accessor :password, :password_confirmation
  attr_accessible :address, :age, :birthday, :first_name, :last_name, :previlage,
                  :email, :username, :password, :password_hash, :password_salt, :password_confirmation
  
  #validation
  validates :password, :presence => {:on => :create},
                       :confirmation => true,
                       :length => {:minimum => 4,:maximum => 32}
                                         
  validates :email,   :presence => true,
                      :uniqueness => true,
                      :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
                      
  validates :username,   :presence => true,
                         :uniqueness => true,
                         :length => {:minimum => 2,:maximum => 20}
                         
  validates :first_name, :presence => true
  validates :last_name, :presence => true
                         
  #relation
  has_many :products,  :dependent => :destroy 
  has_many :articles,  :dependent => :destroy
  belongs_to :country
  
  
  #instance method
  
  #check is instance user is admin
  def is_admin?
    if self.previlage == 1
      true
    else
      false
    end
  end
  
  #return full name of user
  def full_name
    "#{self.first_name} #{self.last_name}"
  end
  
  #show user address and country
  def show_address_country
    "Country Code : #{self.country.code} Country Name : #{self.country.name}"
  end

  #model method
  
  #check is user already registered
  def self.authenticate(username, password)
    user = find_by_username(username)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  #encrypt password field into password salt and hash, for user auth
  def encrypt_password
    if password.present?
          self.password_salt = BCrypt::Engine.generate_salt
          self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

end
