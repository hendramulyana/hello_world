class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :label
      t.string :url
      t.timestamps
    end
  end
end
