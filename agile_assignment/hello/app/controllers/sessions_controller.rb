class SessionsController < ApplicationController
  #show login page
  def new
  end
  
  #user login mechanism
  def create
    user = User.authenticate(params[:username], params[:password])
    if user
      session[:user_id] = user.id
      if user.is_admin?
        redirect_to admin_articles_url, :notice => "Logged in!"
      else
        redirect_to articles_url, :notice => "Logged in!"
      end
    else
      flash[:notice] = "Invalid username or password" 
      render "new"
    end
  end
  
  #user logout mechanism
  def destroy
    session[:user_id] = nil
    redirect_to root_url, :notice => "Logged out!"
  end
end
