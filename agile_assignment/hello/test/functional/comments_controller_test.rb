require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  test "should get create" do
    #add comment with proper param
    login_as(users(:not_admin).username)
    article_id = Article.find(articles(:article1)).id
    post :create, :comment => {:content => "sadf", :article_id => article_id}
    assert_response :redirect
    assert_equal flash[:notice], "Add new comment success"
    
    #check add comment with bad param
    post :create, :comment => {:content => nil, :article_id => article_id}
    assert_response :redirect
    assert_equal flash[:notice], "Add new comment failed"
  end
end
