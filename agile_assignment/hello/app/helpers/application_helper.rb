module ApplicationHelper
  def welcome_text
    str = "" # if the user has logged in, show the welcome text.
    if current_user
      str = "Welcome, #{current_user.email} | "
      str += link_to "Logout", logout_path
    else
      str = "#{link_to "Login", login_path} | "
      str += link_to "Signup", register_path
    end
  end 
end
