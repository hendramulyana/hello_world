# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

articles = Article.create([ {:title => "title1", :body => "body1"}, {:title => "title2", :body => "body2"}, {:title => "title3", :body => "body3"},{:title => "title4", :body => "body4"},{:title => "title5", :body => "body5"}]) 
comments = Comment.create([ {:content => "content1"}, {:content => "content2"}, {:content => "content3"}, {:content => "content4"}, {:content => "conten5"}]) 
countries = Country.create([ {:code => "code1", :name => "name1"},{:code => "code2", :name => "name2"},{:code => "code3", :name => "name3"},{:code => "code4", :name => "name4"},{:code => "code5", :name => "name5"} ]) 
users = User.create([ {:first_name => "first_name1", :last_name => "last_name1", :email => "email1", :username => "username1"},{:first_name => "first_name1", :last_name => "last_name1", :email => "email1", :username => "username1"},{:first_name => "first_name1", :last_name => "last_name1", :email => "email1", :username => "username1"},{:first_name => "first_name1", :last_name => "last_name1", :email => "email1", :username => "username1"},{:first_name => "first_name5", :last_name => "last_name5", :email => "email5", :username => "username5"} ]) 






