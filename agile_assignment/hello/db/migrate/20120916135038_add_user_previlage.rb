class AddUserPrevilage < ActiveRecord::Migration
  def up
    add_column :users, :previlage, :integer
  end

  def down
    remove_column :users, :previlage
  end
end
