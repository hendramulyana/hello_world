class Category < ActiveRecord::Base
   attr_accessible :name
   has_many :products, :through => :join_t_product_categories, :dependent => :destroy
   has_many :join_t_product_categories, :dependent => :destroy
end
