class UsersController < ApplicationController
  
  #user registration page
  def new
    @user = User.new
    @admin_exist = User.where("previlage = 1").select("id").limit(1).count
  end

  #create new user, called from user registration page
  def create
    @user = User.new(params[:user])
    @admin_exist = User.where("previlage = 1").select("id").limit(1).count
    
    if params[:user][:password] != params[:user][:password_confirmation]
      flash[:error] = "Password and Confirmation did not match"
      render "new"
      return
    elsif !verify_recaptcha
      flash[:error] = "Recaptcha did not match with string you entered"
      render "new"
      return
    end
    
    if @user.save
      session[:user_id] = User.find(@user).id
      UserMailer.registration_confirmation(@user).deliver
      redirect_to login_path, :notice => "Registration Complete, Please Login"
    else 
      render "new" 
    end  
  end
end
