class CommentsController < ApplicationController

  #add new comment to article with id of
  def create
    @article = Article.find(params[:comment][:article_id]);
    @comments = @article.comments
    @comment = @article.comments.new(:content => params[:comment][:content])
    
    respond_to do |format|
      if @comment.save
        flash[:notice] = "Add new comment success"
        format.html { redirect_to(articles_detail_path(@article), :notice => 'Add new comment success') }
        format.js
      elsif
        format.html { redirect_to(articles_detail_path(@article), :notice => 'Add new comment failed')}
        flash[:notice] = "Add new comment failed"
      end
    end
  end
end
