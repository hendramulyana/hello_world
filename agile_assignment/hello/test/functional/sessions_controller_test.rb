require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get dstroy" do
    login_as(users(:not_admin).username)
    get :destroy
    assert_response :redirect
    assert_redirected_to root_url
    assert_nil session[:user_id]
  end
  
  test "shold get create" do
    #login as admin
    post :create, {:username => "hendra", :password => "8922"}
    assert_response :redirect
    assert_redirected_to admin_articles_url
    
    #login as user
    post :create, {:username => "maulida", :password => "8922"}
    assert_response :redirect
    assert_redirected_to articles_url
    
    #test login error
    post :create, {:username => "testerror", :password => "error"}
    assert_response :success
    assert_equal flash[:notice], "Invalid username or password"
  end
end
