class Admin::ArticlesController < Admin::ApplicationController
  before_filter :require_admin_login
  
  #show all articles
  def index
    @site_title = "Hello Project - Admin Access - All Article"
  end
  
  #create new article form
  def new
    @article = Article.new
  end

  #edit article form
  def edit
    @article = Article.find(params[:id])
  end
  
  #delete selected article
  def destroy
    if Article.destroy(params[:id])
      flash[:notice] = "Article Deleted Successfuly"
    else  
      flash[:notice] = "Article Delete Failed"
    end
    redirect_to admin_articles_path
  end

  #create new article process, called from new form
  def create
    params[:article][:user_id] = session[:user_id]
    @article = Article.new(params[:article])
    if @article.save
      flash[:notice] = "New Article Created Succesfully"
      redirect_to admin_articles_path
    elsif
      flash[:notice] = "New Article Create Failed"
      render :action => :new
    end
  end

  #update article process, called from edit form
  def update
    if Article.update(params[:id],params[:article])
      flash[:notice] = "Article Edited Succesfully"
      redirect_to admin_articles_path
    elsif
      flash[:notice] = "Article Edit Failed"
      render :action => index
    end
  end
  
  #ahow articles detail and article comment
  def detail
    @article = Article.find(params[:id]);
    @comments = @article.comments
    @comment = Article.find(params[:id]).comments.new
  end
end
