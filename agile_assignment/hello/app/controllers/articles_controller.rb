class ArticlesController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :index, :destroy]
  
  #index page, show a list of all articles
  def index
    @site_title = "Hello Project - All Article"
  end

  #create new article form
  def new
    @article = Article.new
  end

  #edit article form
  def edit
    article = Article.where("id = #{params[:id]} and user_id = #{session[:user_id]}").select("id").limit(1)
    if article.count == 0
      flash[:notice] = "You Don't have authorization to edit this article"
      redirect_to articles_path
    end
    @article = Article.find(params[:id])
  end
  
  #delete selected article
  def destroy
    article = Article.where("id = #{params[:id]} and user_id = #{session[:user_id]}").select("id").limit(1)
    if article.count == 0
      flash[:notice] = "You Don't have authorization to delete this article"
    elsif Article.destroy(params[:id])
      flash[:notice] = "Article Deleted Successfuly"
    else  
      flash[:notice] = "Article Delete Failed"
    end
    redirect_to articles_path
  end

  #create new article process, called from new form
  def create
    params[:article][:user_id] = session[:user_id]
    @article = Article.new(params[:article])
    if @article.save
      flash[:notice] = "New Article Created Succesfully"
      redirect_to new_article_path
    elsif
      flash[:notice] = "New Article Create Failed"
      render :action => :new
    end
  end

  #update article process, called from edit form
  def update
    begin
      Article.update(params[:id],params[:article])
      flash[:notice] = "Article Edited Succesfully"
    rescue => e
      flash[:notice] = "Article Edit Failed"
    end
    redirect_to articles_path
  end
  
  #ahow articles detail and article comment
  def detail
    @article = Article.find(params[:id]);
    @comments = @article.comments
    @comment = Article.find(params[:id]).comments.new
  end
end
