class Country < ActiveRecord::Base
  attr_accessible :name, :code
  has_many :users,  :dependent => :destroy
  
  validates :name, :presence => true
  
  validate :valid_country
  
  #filter valid country to id, usa, and frc
  def valid_country
    self.errors[:name] << "can only be filled with id, usa, and frc" if name != 'id' && name != 'usa' && name != 'frc'
  end 
  
  
end
