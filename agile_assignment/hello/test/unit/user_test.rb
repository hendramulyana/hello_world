require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def test_is_admin?
    assert_equal users(:admin).is_admin?, true 
    assert_equal users(:not_admin).is_admin?, true 
  end
  
  def test_full_name
    user = users(:admin)
    assert_not_nil user.full_name
  end
  
  def test_show_address_country
    user = User.find(users(:admin))
    user.country = Country.find(countries(:country1))
    assert_not_nil user.show_address_country
  end

  def test_self_authenticate
    assert_not_nil User.authenticate(users(:admin).username,"8922")
  end

  def test_encrypt_password
    user = User.new(:username => "testusername",
                    :first_name => "testfirst",
                    :last_name => "testlast",
                    :password => "testpass",
                    :password_confirmation => "testpass",
                    :email => "test@email.com")
    user.save
    assert_not_nil user.password_salt
    assert_not_nil user.password_hash
  end

  def test_create_user
    user = User.new(:username => "testusername2",
                    :first_name => "testfirst2",
                    :last_name => "testlast2",
                    :password => "testpasp2",
                    :password_confirmation => "testpasp2",
                    :email => "test@email.com")
    assert_equal user.save, true
  end
  
  def test_create_user_password
    user = User.new(:username => "testusername3",
                    :first_name => "testfirst3",
                    :last_name => "testlast3",
                    :password => nil,
                    :email => "test@email.com3")
    
    #test create without password
    assert_equal user.save, false
    
    #test create password less than minimum
    user.password = "a"
    assert_equal user.save, false
    
    #test create password more than maximum
    user.password = "ahgfhfkhhfhjfhjfkyfkjkfhfkhkffyfhfhfyfhfhfyfhfhfhvhvhjkfhfhjfhvhvhjfhfhvhfhvhfhfhfhfhjfhfhjfhfhjfhjfhjfhjfhjfhjfhj"
    assert_equal user.save, false
  end
  
  def test_create_user_email
    user = User.new(:username => "testusername4",
                    :first_name => "testfirst4",
                    :last_name => "testlast4",
                    :password => "testpass4",
                    :email => nil)
    
    #test create without email
    assert_equal user.save, false
    
    #test create email not unique
    user.email = "test@email.com"
    User.create(:username => "testusername4",
                :first_name => "testfirst4",
                :last_name => "testlast4",
                :password => "testpass4",
                :email => "test@email.com")
    assert_equal user.save, false
    
    #test create email with malformed format
    user.password = "testemail"
    assert_equal user.save, false
  end

  def test_create_user_username
    user = User.new(:username => nil,
                    :first_name => "testfirst5",
                    :last_name => "testlast5",
                    :password => "testpass5",
                    :email => "test@email.com5")
    
    #test create without username
    assert_equal user.save, false
    
    #test create username with length less than minimum
    user.username = "a"
    assert_equal user.save, false
    
    #test create username with length more than maximum
    user.username = "adffgdagasgdsfdsgdasfdfdsgdsfdsfgafadfdsgdsfdasfdsgsdf"
    assert_equal user.save, false
  end

  def test_create_user_first_last_name
    user = User.new(:username => "testuser6",
                    :first_name => nil,
                    :last_name => "testlast6",
                    :password => "testpass6",
                    :email => "test@email.com6")
    
    #test create without first_name
    assert_equal user.save, false
    
    #test create without last name
    user.first_name = "testfirst6"
    user.last_name = nil
    assert_equal user.save, false
    
  end
  
end
