require 'test_helper'

class CountryTest < ActiveSupport::TestCase
  
  def test_create_country
    country = Country.new(:name => "usa", :code => "usa")
    assert_equal country.save, true
    
    #test not valid country name
    country = Country.new(:name => "sas", :code => "usa")
    assert_equal country.save, false
    
    #test create without name
    country = Country.new(:code => "usa")
    assert_equal country.save, false
  end
  
end
