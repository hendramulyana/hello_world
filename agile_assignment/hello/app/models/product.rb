class Product < ActiveRecord::Base
   attr_accessible :name, :price, :description, :user_id
   belongs_to :user
   
   has_many :categories, :through => :join_t_product_categories, :dependent => :destroy
   has_many :join_t_product_categories, :dependent => :destroy
   
   
   scope :price_morethan_1000, where("price > 1000")
   scope :get_red_product, where("name like '%red'")
end
