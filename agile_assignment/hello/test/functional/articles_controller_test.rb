require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  test "should get index" do
    login_as(users(:not_admin).username)
    Article.find(articles(:article1)).update_attribute(:user_id, users(:not_admin).id)
    get :index
    assert_response :success
  end

  test "should get new" do
    login_as(users(:not_admin).username)
    get :new
    assert_response :success
  end

  test "should get edit" do
    #try to edit without proper ownership
    login_as(users(:not_admin).username)
    get :edit, :id => Article.find(articles(:article1)).id
    assert_response :redirect
    assert_redirected_to articles_path
    
    #edit with proper ownership
    Article.find(articles(:article1)).update_attribute(:user_id, users(:not_admin).id)
    get :edit, :id => Article.find(articles(:article1)).id
    assert_response :success
  end

  test "should get destroy" do
    #try to destroy without proper ownership
    login_as(users(:not_admin).username)
    get :destroy, :id => Article.find(articles(:article1)).id
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:notice], "You Don't have authorization to delete this article"
    
    #destroy with proper ownership
    Article.find(articles(:article1)).update_attribute(:user_id, users(:not_admin).id)
    get :destroy, :id => Article.find(articles(:article1)).id
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:notice], "Article Deleted Successfuly"
  end

  test "should get create" do
    #try to create new article with proper param
    login_as(users(:not_admin).username)
    post :create, :article => {:title => "sadf", :body => "afdg", :rating => 123}
    assert_response :redirect
    assert_redirected_to new_article_path
    assert_equal flash[:notice], "New Article Created Succesfully"
    
    #try to create new article with bad param
    articles(:article1).title = ""
    post :create, :article => {:body => "", :rating => 123}
    assert_response :success
    assert_equal flash[:notice], "New Article Create Failed"
  end

  test "should get update" do
    #try to update article with proper param
    login_as(users(:not_admin).username)
    article = Article.find(articles(:article1))
    post :update, {:article => {:title => "sadf", :body => "afdg", :rating => 123}, :id => article.id}
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:notice], "Article Edited Succesfully"
    
    #try to create new article with bad param
    articles(:article1).title = ""
    post :update, {:article => {:title => "", :body => "", :rating => "a"}, :id => 423223}
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:notice], "Article Edit Failed"
  end

  test "should get detail" do
    login_as(users(:not_admin).username)
    post :detail, :id => Article.find(articles(:article1)).id
    assert_response :success
  end
  
end
