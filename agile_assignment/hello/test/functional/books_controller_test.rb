require 'test_helper'

class BooksControllerTest < ActionController::TestCase
  test "should get gallery" do
    get :gallery
    assert_response :success
  end

  test "should get author" do
    get :author
    assert_response :success
  end

end
